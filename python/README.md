# To build and install the extension

`python setup.py install`

# To clean up

`python setup.py clean -a`

# To clean up even more

`git clean -f -xd`

