import numpy as np
from sklearn.base import ClassifierMixin, BaseEstimator
from mwumkl import test_mkl, train_mwu_mkl

class BaseMKL(object):
    def __init__(self):
        self.feature_spec = []
        self.param_spec = []
        self.kern_spec = []

    def add_kernel(self, type, **params):
        feature = 'all'
        if 'each' in params:
            feature = 'each'
        if 'all' in params:
            feature = 'all'

        if type in ['LINEAR']:
            self.add_linear(feature, **params)
        elif type in ['POLY']:
            self.add_poly(feature, **params)
        elif type in ['RBF', 'GAUSSIAN']:
            self.add_gaussian(feature, **params)
        elif type in ['TANH', 'LOGISTIC', 'SIGMOID']:
            self.add_tanh(feature, **params)

    def add_linear(self, feature, **unused):
        self.feature_spec.append(feature)
        self.kern_spec.append(0)
        self.param_spec.append(0.0)

    def add_poly(self, feature, degree, **unused):
        for d in degree:
            self.feature_spec.append(feature)
            self.kern_spec.append(1)
            self.param_spec.append(d)

    def add_gaussian(self, feature, gamma, **unused):
        for g in gamma:
            self.feature_spec.append(feature)
            self.kern_spec.append(2)
            self.param_spec.append(1.0/g)

    def add_tanh(self, feature, gamma, **unused):
        for g in gamma:
            self.feature_spec.append(feature)
            self.kern_spec.append(4)
            self.param_spec.append(g)

    def realize_config(self, d):
        fsel = dict(all=-1, each=np.int32(range(d)))
        psel = dict(all=1, each=np.ones((d,), dtype=np.double))
        ksel = dict(all=1, each=np.ones((d,), dtype=np.int))

        features = np.hstack(fsel[f] for f in self.feature_spec)
        params = np.hstack(psel[f]*p for f,p in 
                           zip(self.feature_spec, self.param_spec))
        kerns = np.hstack(ksel[f]*k for f,k in 
                          zip(self.feature_spec, self.kern_spec))

        return (features, params, kerns)

class MWUMKL(BaseMKL, BaseEstimator, ClassifierMixin):
    """xxx"""
    
    def __init__(self, epsilon=0.2, C=1000.0, verbose=0):
        super(MWUMKL,self).__init__()

        self.epsilon = epsilon
        self.C = C
        self.verbose = verbose

        self.features_ = np.int32([])
        self.params_ = np.double([])
        self.kerns_ = np.int32([])

        self.Sigma = np.double([])
        self.alpha = np.double([])
        self.bias = 0
        self.supp = np.int32([])

        self.Xs_ = np.double([])
        self.ys_ = np.double([])

    def fit(self, X, y):
        n,d = X.shape

        self.features_, self.params_, self.kerns_ = self.realize_config(d)

        success, Sigma, alpha, bsvm, posw = train_mwu_mkl(self.kerns_, self.params_, 
                                                          self.features_, X.T, y, 
                                                          self.epsilon, self.C, 2,
                                                          self.verbose)

        if self.verbose > 0:
            print 'fit',
            print 'X', X.shape,
            print 'y', y.shape,
            print 'success', success
        
        if success > 0:
            self.Sigma = Sigma
            self.alpha = alpha
            self.bias = bsvm
            self.supp = (alpha > 0.0)
            self.Xs_ = X[self.supp,:]
            self.ys_ = y[self.supp]

            if self.verbose > 0:
                print 'fit success:',
                print 'bias', self.bias,
                print 'sigma', self.Sigma.shape,
                print 'alpha', self.alpha.shape,
                print 'supp', self.supp.shape,
                print 'Xs', self.Xs_.shape,
                print 'ys', self.ys_.shape

            self.last_result = None
            self.last_X = None

            return self
        else:
            return None

    def decision_function(self, X):
        if self.verbose > 0:
            print 'decision_function:',
            print 'bias', self.bias,
            print 'X', X.shape,
            print 'Xs', self.Xs_.shape,
            print 'ys', self.ys_.shape

        if self.last_result != None:
            if self.last_X is X:
                return self.last_result

        self.last_X = X
        self.last_result = test_mkl(self.Sigma, self.alpha[self.supp], 
                                    self.kerns_, self.params_, self.features_, 
                                    self.Xs_.T, X.T, self.ys_, self.verbose) + self.bias

        return self.last_result

    def predict(self, X):
        return np.sign(self.decision_function(X))
