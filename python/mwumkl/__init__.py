from native import train_mwu_mkl, test_mkl

from skl import MWUMKL

from pkg_resources import get_distribution
__version__ = get_distribution(__package__).version
