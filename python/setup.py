#! /usr/bin/env python

# System imports
from distutils.core import setup, Extension

# Third-party modules
import numpy

# Obtain the numpy include directory.
numpy_include = numpy.get_include()

_mwumkl = Extension("mwumkl.native._native",
                    ["mwumkl/native/mwu_main.i",
                     "../mwu_dynamic.cpp"],
                    depends = ["../kernel.hpp", 
                               "../mwu_main.h"],
                    include_dirs = [numpy_include, 
                                    '..'],
                    swig_opts = ['-c++'])

# mwumkl setup
setup(name        = "mwumkl",
      version     = "0.1.0",
      description = "MWU-MKL Algorithm",
      author      = "John Moeller",
      packages    = ["mwumkl", "mwumkl.native", "mwumkl.skl"],
      ext_modules = [_mwumkl])
